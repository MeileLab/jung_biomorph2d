/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2017 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Main author: Wim Degruyter */


/* ==================================================================================================================================
"Upscaling of microbially driven first-order reactions in heterogeneous porous media"
the palabos code modified from permeability example

by Heewon Jung (Heewon.Jung@uga.edu)

Geometry material numbers
    0 = fluid
    1 = grain boundary (bounce back)
    2 = biomass locaation (NS: bounce back, ADE: diffusion)
    5 = do nothing (No dynamics)
 ================================================================================================================================== */

#include "palabos2D.h"
#include "palabos2D.hh"
#include <vector>
#include <cmath>
#include <cstdlib>
#include <sys/stat.h>
#include <ctime>
#include <stack>

using namespace plb;
typedef double T;

#define NSDES descriptors::MRTD2Q9Descriptor
#define RXNDES descriptors::AdvectionDiffusionD2Q5Descriptor

// a function to initially distribute the pressure linearly.
// This is used only to initializing the flow field (functional: NSdomainSetup)
class PressureGradient {
public:
    PressureGradient(T deltaP_, plint nx_) : deltaP(deltaP_), nx(nx_)
    { }
    void operator() (plint iX, plint iY, T& density, Array<T,2>& velocity) const
    {
        velocity.resetToZero();
        density = (T)1 - deltaP*NSDES<T>::invCs2 / (T)(nx-1) * (T)iX;

    }
private:
    T deltaP;
    plint nx;
};

// load a geometry file with predefined material numbers
void readGeometry(std::string fNameIn, MultiScalarField2D<int>& geometry)
{

    pcout << "Reading the geometry file." << std::endl;
    const plint nx = geometry.getNx();
    const plint ny = geometry.getNy();

    Box2D sliceBox(0,0, 0,ny-1);
    std::auto_ptr<MultiScalarField2D<int> > slice = generateMultiScalarField<int>(geometry, sliceBox);

    plb_ifstream geometryFile(fNameIn.c_str());
    for (plint iX=0; iX<nx; ++iX) {
        if (!geometryFile.is_open()) {
            pcout << "Error: could not open geometry file " << fNameIn << std::endl;
            exit(EXIT_FAILURE);
        }
        geometryFile >> *slice;
        copy(*slice, slice->getBoundingBox(), geometry, Box2D(iX,iX, 0,ny-1));
    }

    {
        VtkImageOutput2D<T> vtkOut("porousMedium", 1.0);
        vtkOut.writeData<float>(*copyConvert<int,T>(geometry, geometry.getBoundingBox()), "tag", 1.0);
    }
}

// Flow domain boundary conditions.  No flow boundaries at the top and bottom.
void NSdomainSetup(MultiBlockLattice2D<T,NSDES>& lattice,
                    OnLatticeBoundaryCondition2D<T,NSDES>* boundaryCondition,
                    MultiScalarField2D<int>& geometry, T deltaP)
{
    const plint nx = lattice.getNx();
    const plint ny = lattice.getNy();

    Box2D west   (0,0, 0,ny-1);
    Box2D east   (nx-1,nx-1, 0,ny-1);

    boundaryCondition->addPressureBoundary0N(west, lattice);
    setBoundaryDensity(lattice, west, (T) 1.);

    boundaryCondition->addPressureBoundary0P(east, lattice);
    setBoundaryDensity(lattice, east, (T) 1. - deltaP*NSDES<T>::invCs2);

    defineDynamics(lattice, geometry, new BounceBack<T,NSDES>(), 1);
    defineDynamics(lattice, geometry, new BounceBack<T,NSDES>(), 2);
    defineDynamics(lattice, geometry, new NoDynamics<T,NSDES>(), 3);
    defineDynamics(lattice, geometry, new NoDynamics<T,NSDES>(), 5);

    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), PressureGradient(deltaP, nx));
    
    lattice.initialize();
    delete boundaryCondition;
}

// solute domain boundary conditions. No flow boundaries at the top and bottom.
void AVDdomainSetup(MultiBlockLattice2D<T,RXNDES> &lattice,
                        OnLatticeAdvectionDiffusionBoundaryCondition2D<T, RXNDES>* boundaryCondition,
                        MultiScalarField2D<int>& geometry, T rho0, T rhoInlet, T bMassOmega)
{
    const plint nx = lattice.getNx();
    const plint ny = lattice.getNy();

    Box2D west   (0,0, 0,ny-1);
    Box2D east   (nx-1,nx-1, 0,ny-1);
    plint processorLevelBC = 1;
  
    boundaryCondition->addTemperatureBoundary0N(west, lattice);
    setBoundaryDensity(lattice, west, rhoInlet);

    boundaryCondition->addTemperatureBoundary0P(east, lattice);
    integrateProcessingFunctional(new FlatAdiabaticBoundaryFunctional2D<T,RXNDES, 0, +1>, east, lattice, processorLevelBC);
    // FlatAdiabaticBoundaryFunctional2D copies the population at "Box2D right" and pastes to the right"+1" location in x-direction ("0")
    // integrateProcessingFunctional is a functional being executed after collision and streaming steps.
    // processorLevelBC specifies the execution order of internal data processors. See the palabos manual for more details.

    defineDynamics(lattice, geometry, new BounceBack<T,RXNDES>(), 1);
    defineDynamics(lattice, geometry, new AdvectionDiffusionRLBdynamics<T,RXNDES>(bMassOmega), 2);
    defineDynamics(lattice, geometry, new NoDynamics<T,RXNDES>(), 3);
    defineDynamics(lattice, geometry, new NoDynamics<T,RXNDES>(), 5);

    Array<T,2> u0(0., 0.);
    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), rho0, u0);

    lattice.initialize();
    delete boundaryCondition;
}

// writing output files of the NS domain
void writeNsVTK(MultiBlockLattice2D<T,NSDES>& lattice, plint iter)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtkVelcity", iter, 7), 1.);

    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", 1.);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", 1.);
}

// writing output files of the AVD domain
void writeAdvVTK(MultiBlockLattice2D<T,RXNDES>& lattice, plint iter, std::string nameid)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtkDensity" + nameid, iter, 7), 1.);
    vtkOut.writeData<T>(*computeDensity(lattice), "Density", 1.);
}


// ========================================== DATA PROCESSOR ========================================== //

template<typename T, template<typename U> class Descriptor>
class ComputeLatticeDottedRho2D : public DotProcessingFunctional2D_L<T,Descriptor>
{
public:
    ComputeLatticeDottedRho2D(T dt_, T k_)
    : dt(dt_), k(k_)
    {}
    virtual void process(DotList2D const& dotList, BlockLattice2D<T,Descriptor> &lattice) {
        Array<T,5> g;
        for (plint iDot=0; iDot < dotList.getN(); ++iDot) {
            // dotList is a list storing the locations of intests in the simulation domain.
            // Here, the locations with material number 2 are used for a consumption reaction
            Dot2D const& dot = dotList.getDot(iDot);
            lattice.get(dot.x,dot.y).getPopulations(g);
            T c0 = lattice.get(dot.x,dot.y).computeDensity();
            T cdot = k*c0;
            g[0]+=(T) (cdot/3); g[1]+=(T) (cdot/6); g[2]+=(T) (cdot/6); g[3]+=(T) (cdot/6); g[4]+=(T) (cdot/6);
            lattice.get(dot.x,dot.y).setPopulations(g);
        }
    }
    virtual ComputeLatticeDottedRho2D<T,Descriptor>* clone() const {
        return new ComputeLatticeDottedRho2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
private:
    T dt;
    T k;
};

// ========================================== MAIN PROGRAM ========================================== //

int main(int argc, char **argv)
{
    plbInit(&argc, &argv);

    if (argc!=11) {
        pcout << "Error missing some input parameter\n";
        pcout << "The structure is :\n";
        pcout << "1. Input geometry file name.\n";
        pcout << "2. nx.\n";
        pcout << "3. ny.\n";
        pcout << "4. resolution (grid spacing) in microns.\n";
        pcout << "5. deltaP.\n";
        pcout << "6. Reynolds number.\n";
        pcout << "7. Peclet number.\n";
        pcout << "8. Damkohler number.\n";
        pcout << "9. Continuous Simulation (use the field but no = 2,  yes = 1, no = 0).\n";
        pcout << "10. Flow Simulation only (yes = 1, no = 0).\n";
        pcout << "Example: " << argv[0] << " geometry.dat 1001 501 10 0.001 1 1 2 1\n";
        exit (EXIT_FAILURE);
    }

    std::string fNameIn  = argv[1];
    const plint nx = atof(argv[2]);
    const plint ny = atof(argv[3]);
    const T dx = atof(argv[4]) * 1e-6;
    const T deltaP = atof(argv[5]);
    const T Re = atof(argv[6]);
    const T Pe = atof(argv[7]);
    const T Da = atof(argv[8]);
    const plint contSim = atof(argv[9]);
    const bool tick = atof(argv[10]);

    struct stat statStruct;
    const char *dirname = "output/";
 
    stat(dirname, &statStruct);
 
    if (S_ISDIR(statStruct.st_mode)) {
        pcout << "output folder already exists" << std::endl;
    }
    else {
        pcout << "Creating output folder (./output)" << std::endl;
        mkdir(dirname, 0777);
    }
    global::directories().setOutputDir(dirname);

    pcout << "Read geometry file." << std::endl << std::endl;
    MultiScalarField2D<int> geometry(nx,ny);
    readGeometry(fNameIn, geometry);

    // create the dotLists.
    DotList2D dotList2;
    plint len0=0, len2=0;
    for (plint iX=0; iX<nx; ++iX) {
        for (plint iY=0; iY<ny; ++iY) {
            if (geometry.get(iX,iY)==0) {
                dotList0.addDot(Dot2D(iX,iY));
                ++len0;
            }
            else if (geometry.get(iX,iY)==2) {
                dotList2.addDot(Dot2D(iX,iY)); // this is used to update the locat substrate concentration.
                ++len2;
            }
        }
    }
    plint poroLen = len0 + len2; // this is for calculating the rate constant for a fixed Da

// =================================== nsLattice setup =================================== //     

    const T lx = dx * nx, ly = dx * ny;
    const T latticeLenC = 29.4; // average pore diameter
    const T physLenC = latticeLenC * dx;
    const T nsLatticeTau = 1;
    const T nsLatticeOmega = 1 / nsLatticeTau;
    const T nsLatticeNu = NSDES<T>::cs2*(nsLatticeTau-0.5);
    const T physNu = 1e-6; // m2/s

    const T nsLatticeVc = 0.0058;
    const T physVc = Re * physNu / physLenC;
    const T ns_dt = nsLatticeVc * dx / physVc;

    pcout << "nx = " << nx << ", ny = " << ny << ", lx = " << lx << " m, ly = " << ly << " m, dx = " << dx << "(m), ns_dt = " << ns_dt << " s" << std::endl;
    pcout << "characteristic length = " << latticeLenC << ", characteristic velocity = " << physVc << " m/s." << std::endl << std::endl;
    pcout << "nsLatticeTau = " << nsLatticeTau <<", nsLatticeOmega = " << nsLatticeOmega << ", nsLatticeNu = " << nsLatticeNu << std::endl << std::endl;

    pcout << "Instantiate the fluid lattice." << std::endl;
    MultiBlockLattice2D<T,NSDES> nsLattice(nx, ny, new MRTdynamics<T,NSDES>(nsLatticeOmega));
    NSdomainSetup(nsLattice, createLocalBoundaryCondition2D<T,NSDES>(), geometry, deltaP);

/*  =================================== NS Main Loop ===================================  */
    int iT = 0; bool nsSim=0;
    const plint nsiTmax  = 10000000;
    const char *NSname = "./output/nsLatticeCheckPoint.dat";
    util::ValueTracer<T> converge(1.0,1000.0,1.e-10);

    pcout << "LBM NS simulation begins \n \n";
    pcout << "run main nsLattice loop" << std::endl;
    for (; iT < nsiTmax; ++iT) {
        nsLattice.collideAndStream();
        converge.takeValue(getStoredAverageEnergy(nsLattice),true);
        if (converge.hasConverged()) {
            break;
        }
    }
    pcout << std::endl << "flow calc finished at iT = " << iT << std::endl;
    pcout << "Writing velocity VTK... \n";
    writeNsVTK(nsLattice,nsiTmax);
    pcout << "Writing checkpoint... \n\n";
    saveBinaryBlock(nsLattice, NSname); // palabos binary block of the NS lattice

    T PoreMeanU = computeAverage(*computeVelocityNorm(nsLattice, Box2D (1,nx-2,0,ny-1)), geometry, 0);
    T PoreMaxUx = computeMax(*computeVelocityComponent(nsLattice, Box2D (1,nx-2,0,ny-1), 0));
    T DarcyOutletUx = computeAverage(*computeVelocityComponent(nsLattice, Box2D (nx-2,nx-2, 0,ny-1), 0));
    T DarcyMiddleUx = computeAverage(*computeVelocityComponent(nsLattice, Box2D ((nx-1)/2,(nx-1)/2, 0,ny-1), 0));
    T DarcyInletUx = computeAverage(*computeVelocityComponent(nsLattice, Box2D (1,1, 0,ny-1), 0));

    pcout << "Outlet Darcy Ux = " << DarcyOutletUx << std::endl;
    pcout << "Middle Darcy Ux = " << DarcyMiddleUx << std::endl;
    pcout << "Inlet Darcy Ux = " << DarcyInletUx << std::endl;
    pcout << "CFL number (= maximum local lattice velocity)= " << PoreMaxUx << std::endl;
    pcout << "Mach number = " << PoreMaxUx/sqrt(RXNDES<T>::cs2) << std::endl  << std::endl;

// =================================== rxnLattice setup =================================== //     
    const T physD = 1e-9; // solute diffusivity in pore water
    const T withinBMassD = 0.8*physD; // solute diffusivity in biomass voxels 
    const T bMassNum = 2e5; // total biomass

    const T rxnLatticeNu = DarcyOutletUx * latticeLenC / Pe;
    const T rxnLatticeTau = rxnLatticeNu * RXNDES<T>::invCs2 + 0.5;
    const T rxnLatticeOmega = 1/rxnLatticeTau;

    const T rxnLatticebMassNu = rxnLatticeNu * withinBMassD / physD;
    const T rxnLatticebMassTau = rxnLatticebMassNu * RXNDES<T>::invCs2 + 0.5;
    const T rxnLatticebMassOmega = 1/rxnLatticebMassTau;

    const T rateConst = Da * rxnLatticeNu / latticeLenC / latticeLenC / (bMassNum/poroLen);
    
    const T localRateConst = rateConst*(bMassNum/len2);

    pcout << "rxnLatticeTau = " << rxnLatticeTau << ", rxnLatticebMassTau = " << rxnLatticebMassTau << std::endl;
    pcout << "rxnLatticeOmega = " << rxnLatticeOmega << ",rxnLatticebMassOmega = " << rxnLatticebMassOmega << std::endl;
    pcout << "rxnLatticeNu = " << rxnLatticeNu << ", rxnLatticebMassNu = " << rxnLatticebMassNu << std::endl << std::endl;

    pcout << "Peclet Number (outlet) = " << DarcyOutletUx * latticeLenC / rxnLatticeNu << std::endl;
    pcout << "Peclet Number (meanU) = " << PoreMeanU * latticeLenC / rxnLatticeNu << std::endl;
    pcout << "grid Peclet Number (maxU) = " << PoreMaxUx / rxnLatticeNu << std::endl;
    pcout << "Diffusive Damkohler number = " << Da << ", rate constant = " << rateConst << std::endl;
    pcout << "local rate constant = " << localRateConst << std::endl << std::endl;

    if (tick == 1) {
        pcout << "Run the NS simualtion part only." << std::endl;
        exit(EXIT_FAILURE);
    }

    pcout << "Instantiate the reaction lattices." << std::endl;
    MultiBlockLattice2D<T,RXNDES> rxnLatticeA(nx, ny, new AdvectionDiffusionRLBdynamics<T,RXNDES>(rxnLatticeOmega));

    AVDdomainSetup(rxnLatticeA, createLocalAdvectionDiffusionBoundaryCondition2D<T,RXNDES>(), geometry, 0., 1., rxnLatticebMassOmega); //geometry, initial conc., boundary conc., bMassOmega

/*  ===============================================          ADV Main Loop         ====================================================  */

    // Couple the two lattices
    latticeToPassiveAdvDiff(nsLattice, rxnLatticeA, rxnLatticeA.getBoundingBox());

    iT = 0;
    const plint adviTmax = 30000000;
    plint iTNum1 = 500000;
    plint iTNum2 = 5000000;

    pcout << "LBM ADRE simulation begins" << std::endl << std::endl;
    time_t tstart, tend;
    tstart=time(0);

    for (; iT < adviTmax; ++iT) {

        if (iT % iTNum1 == 0) {
            pcout << "Iteration = " << iT << std::endl;
            pcout << "Writing concentration VTK... \n";
            writeAdvVTK(rxnLatticeA, iT, "A");
        }

        if (iT % iTNum2 == 0) {
            pcout << "Writing checkpoint... \n";
            saveBinaryBlock(rxnLatticeA, "./output/DensityAcheckpoint" + std::to_string(iT) + ".dat");

            tend=time(0);
            pcout << "Time elapsed: " << difftime(tend, tstart) << "seconds." << std::endl;
            tstart=time(0);
        }

        rxnLatticeA.collide();
        applyProcessingFunctional(new ComputeLatticeDottedRho2D<T,RXNDES> (1.0, localRateConst), dotList2, rxnLatticeA);
        rxnLatticeA.stream();

    } // end of the main loop

    pcout << "End of simulation at iteration " << iT << std::endl;
    pcout << "Writing the final VTK file ..." << std::endl << std::endl;
    writeAdvVTK(rxnLatticeA, iT, "A");

    pcout << "Writing the final checkpoint... \n";
    saveBinaryBlock(rxnLatticeA, "./output/FianlDensityAcheckpoint" + std::to_string(iT) + ".dat");

    pcout << "Finished!" << std::endl << std::endl;

// ====================      End of the main loop      ======================== 

    return 0;

}
